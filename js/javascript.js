  // Sidebar script
    function openNav() {
        document.getElementById("mySidebar").style.width = "350px";
        document.getElementById("main").style.marginLeft = "250px";
      }
      
      function closeNav() {
        document.getElementById("mySidebar").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
      }

      // Navbar kleur veranderen als je scroll
      $(document).ready(function() {
        $(window).scroll(function() {
          var scroll = $(window).scrollTop();
          if(scroll>50){
            $("#nav").css("background","#1C2432");
          }
          else {
            $("#nav").css("background","transparent");
          }
        });
      });

      // JS carrousel